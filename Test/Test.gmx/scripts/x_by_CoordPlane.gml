/// argument0 = Coord Plane instance id

var r=sqrt(power(x, 2)+power(y, 2));
var ang=arccos(x/r);
return r*cos(ang-argument0.image_angle)-argument0.x;
