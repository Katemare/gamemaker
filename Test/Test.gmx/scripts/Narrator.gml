/// narrate_scoring(argument0=winner)
var winner_title, loser_title;
if argument0==SIDE_LEFT
{
    loser_title=RIGHT_PLAYER_TITLE;
    winner_title=LEFT_PLAYER_TITLE;
}
else
{
    loser_title=LEFT_PLAYER_TITLE;
    winner_title=RIGHT_PLAYER_TITLE;
}

var random_message=irandom(array_length_1d(Narration.messages)-1);
random_message=Narration.messages[random_message];
random_message=string_replace(random_message, '%loser', loser_title);
random_message=string_replace(random_message, '%winner', winner_title);
Narration.my_text=random_message;
