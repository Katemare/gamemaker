######################
### Darkness Comes ###
######################

You are a lonely man in an empty mansion. The lights went out.
All your past sins have come to get you.

## Controls ##

# In-game
Movement: 	arrows, WSAD
Shoot:		Space, click
Reload:		R, middle mouse button
Pause		P
Quit to title	Esc

# Global
Instructions	F1
Language	L
Full screen	Alt+Enter

# Title screen
Start game	Space, Enter
Quit		Esc

## Credits

Graphics & Programming	Anonymous I
SFX			Taira Komori's Japanese Free Sound Effects
			 http://taira-komori.jpn.org/freesounden.html
			Fr3yr (FreeSounds)
Music			Eric Skiff
			 http://ericskiff.com/music/
QA			Anonymous II

Software used		Game Maker: Studio 1.4
			MP3Gain

Produced for Light Academy "Game of the Week" anonymous game jam, Oct 2015
http://rpg-maker.info
Licensed under Creative Commons 3.0 Attribution licence