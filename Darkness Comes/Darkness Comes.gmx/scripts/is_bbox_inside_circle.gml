var target=argument0, circle=argument1;
if !is_point_inside_circle(target.bbox_left, target.bbox_top, circle) return false;
if !is_point_inside_circle(target.bbox_left, target.bbox_bottom, circle) return false;
if !is_point_inside_circle(target.bbox_right, target.bbox_top, circle) return false;
if !is_point_inside_circle(target.bbox_right, target.bbox_bottom, circle) return false;
return true;
