var point_x=argument0, point_y=argument1, circle=argument2;

return point_distance(point_x, point_y, circle.x, circle.y)<=circle.r;
